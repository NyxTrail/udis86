:title: udcli

NAME
====
udcli - A CLI tool for quick disassembly of x86 and x86-64 class instruction set architectures.

SYNOPSIS
========
**udcli** [-option(s)] file

DESCRIPTION
===========
**udcli** is a CLI front-end to the Udis86 Disassembler library.

OPTIONS
=======
**-16**
   Set the disassembly mode to 16 bits.

**-32**
   Set the disassembly mode to 32 bits. (default)

**-64**
   Set the disassembly mode to 64 bits.

**-intel**
   Set the output to INTEL (NASM like) syntax. (default)

**-att**
   Set the output to AT&T (GAS like) syntax.

**-v <v>**
   Set vendor. <v> = {intel, amd}.

**-o <pc>**
   Set the value of the program counter to <pc>. (default = 0)

**-s <n>**
   Set the number of bytes to skip before disassembly to <n>.

**-c <n>**
   Set the number of bytes to disassemble to <n>.

**-x**
   Set the input mode to whitespace separated 8-bit numbers in hexadecimal
   representation.
   Example: 0f 01 ae 00

**-noff**
   Do not display the offset of instructions.

**-nohex**
   Do not display the hexadecimal code of instructions.

**-eflags**
   Display information on EFLAGS register.

**-access**
   Display access information on operand.

**-implicit**
   Display implicit registers used or modified by the instruction.

**-h**
   Display this help message.

**--version**
   Show version

Example
=======
Usage example::

    echo "65 67 89 87 76 65 54 56 78 89 09 00 90" | udcli -32 -x

will result in output such as this::

    0000000080000800 656789877665     mov [gs:bx+0x6576], eax
    0000000080000806 54               push esp
    0000000080000807 56               push esi
    0000000080000808 7889             js 0x80000793
    000000008000080a 0900             or [eax], eax
    000000008000080c 90               nop

Author
======
This version of Udis86 is based on a fork of the original project of the same name by
`Willem (canihavesomecoffee) <https://github.com/canihavesomecoffee>`_. Udis86 was 
originally written by `Vivek Thampi<https://github.com/vmt/>`_.
